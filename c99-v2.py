#-----install-----
#sudo pip3 install selenium
#sudo apt install libxkbcommon0
#sudo apt install chromium-chromedriver
#-----install-----
#usage: python3 c99-v2.py starbucks.com
import re
import sys
import time
import requests
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from concurrent.futures import ThreadPoolExecutor


def find_subdomains(subs):

        try:
                #eğer programı arka arkaya ikinci defa çalıştırdığında selenium hataları alırsan
                #ilk tarihleri almak için subdomainfinder.c99.nl gönderdiğimiz request de firewall
                #devreye girdiği için gerekli elementleri alamıyor diye hata veriyor.
                #bu sebeble bu kısıma time.sleep ekledim ve threadi 1 olarak ayarladım ki firewall
                #ekranına düşmesin diye. eğer hata almaya devam edersen sayfasının title'sını kontrol et
                #bu fonksiyon altındaki driver.get isteğinin altına print("title:",driver.title) komutunu
                #ekle ve programı o şekilde çalıştır. başarılı olduğunda title farklı, firewall devreye girdiğinde
                #title farklı olacaktır. eğer title firewall olursa time.sleep değerini yükselt ve yeniden test et.
                time.sleep(1)
                driver.get(subs)
                #print(driver.title)
                response = driver.execute_script("return document.body.innerHTML")
                total_subs.extend(re.findall(domain_regex, response))
                total_ips.extend(re.findall(r"""href=['"]/geoip/(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})['"]""", response))
        except Exception as e:
                print(e)


test_request = requests.get("https://subdomainfinder.c99.nl/")
if test_request.status_code == 403:
        print("Hedef Sunucu Tarafından İp Adresinin Engellenmis: 403")
        sys.exit()

domain_name = str(sys.argv[1])
total_ips = []
domain_regex = "[a-zA-Z0-9._-]+{}".format(re.escape("." + domain_name))
total_subs = []
url_adresleri = []

options = Options()
options.add_argument("--headless")
options.add_argument("--no-sandbox")
options.add_argument('--disable-dev-shm-usage')
driver = webdriver.Chrome(options=options)
driver.implicitly_wait(20)
driver.get("https://subdomainfinder.c99.nl/")
domain_input = driver.find_element(By.ID, "domain")
domain_input.send_keys(domain_name)
submit_button = driver.find_element(By.ID, "scan_subdomains")
submit_button.click()
response = driver.execute_script("return document.body.innerHTML")

if "No subdomains found." in response:
        print("Veritabanında subdomainler yoktur")
        sys.exit()

tarihler = re.findall(r'\[[0-9][0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]\] '+f"{re.escape(domain_name)}",response)
tarihler = list(set(tarihler))

if not len(tarihler) >0:
        print("tarihler bulunamadı")
        sys.exit()

print("total tarih: {}".format(len(tarihler)))

for urlss in tarihler:
        url_adresleri.append(f"https://subdomainfinder.c99.nl/scans/{urlss[1:11]}/{domain_name}")

with ThreadPoolExecutor(max_workers=1) as executor:
        executor.map(find_subdomains, url_adresleri)


total_subs = list(set(total_subs))
total_ips = list(set(total_ips))
total_subs.sort()
total_ips.sort()

print("total subs: {}".format(len(total_subs)))
print("total ips: {}\n\n".format(len(total_ips)))

for i in total_ips:
        print(i)

for i in total_subs:
        print(i)

with open("c99-output.txt", "w") as file:
        for item in total_subs:
                file.write(item + "\n")