import os
import sys
import argparse

def run_rustscan(hostname):

        try:

                check = os.popen(f'rustscan -a "{hostname}" -b 65535 --ulimit 70000 --timeout 4000| grep Open').read()

                if check:

                        parse = check.split("\n")
                        parse2 = list(filter(None,parse))

                        for replace_host in parse2:

                                combine = f"{hostname}:{replace_host.split(':')[-1]}"

                                print(combine)

                                if args.output:

                                        with open(args.output, "a+") as file:
                                                file.write(combine + "\n")

        except Exception as e:
                print(e)


def startt():


        if not os.path.exists("/usr/bin/rustscan"):
                print("RustScan Not Found In This Path: /usr/bin/rustscan")
                sys.exit()

        targets = []

        if args.list and not args.stdin:

                if not os.path.exists(args.list):
                        print("Target List Not Found")
                        sys.exit()

                file = open(args.list, "r", encoding="utf-8", errors="ignore").read().lower().split("\n")
                targets.extend(list(set(filter(None, file))))

                del file

                if not len(targets) > 0:
                        print("Your Target List Is Empty")
                        sys.exit()


        elif args.stdin and not args.list:

                file = sys.stdin.read().lower().split("\n")

                targets.extend(list(set(filter(None, file))))

                del file

                if not len(targets) > 0:

                        print("Targets Could Not Be Read From Stdin")
                        sys.exit()

        else:
                print("You Need To Use --list Or --stdin Parameter")
                sys.exit()


        targets.sort()


        for host_name in targets:
                run_rustscan(host_name)

if __name__ == "__main__":

        ap = argparse.ArgumentParser()
        ap.add_argument("-l", "--list", required=False, metavar="", help="Target List Path")
        ap.add_argument("-s", "--stdin", required=False, action="store_true", help="Read Targets From Stdin")
        ap.add_argument("-o", "--output", required=False, metavar="", help="Save Output")
        args = ap.parse_args()

        startt()
