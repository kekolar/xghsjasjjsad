#uzak sunucuda kayıtlı c99 programını çalıştırma
import paramiko
import sys


# SSH bağlantı bilgileri
hostname = ''  # Uzak sunucunun IP adresi
port = 22  # SSH portu, genellikle 22
username = ''  # Uzak sunucu kullanıcı adı
password = ''  # Uzak sunucu şifresi

target = sys.argv[1]
# Uzak sunucuda çalıştırılacak Python komutu
command = f'python3 /root/c99.py {target} | grep {target}'

client = None  # client nesnesi başlangıçta None
stdin = stdout = stderr = None  # Diğer nesneleri başlangıçta None

try:
    # SSH istemcisi oluşturma ve bağlantı kurma
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    # SSH bağlantısı (timeout olmadan)
    client.connect(hostname, port=port, username=username, password=password)

    # Komut çalıştırma ve çıktıyı alma
    stdin, stdout, stderr = client.exec_command(command)  # Timeout olmadan komut çalıştırma

    # Çıktıları okuma
    output = stdout.read().decode()
    error = stderr.read().decode()

    # Çıktıları yazdırma
    if output:
        print(output)

except Exception as e:
    pass
finally:
    # Bağlantıyı her durumda kapat
    if client is not None:
        client.close()
        # Nesneleri sil
        del client, stdin, stdout, stderr